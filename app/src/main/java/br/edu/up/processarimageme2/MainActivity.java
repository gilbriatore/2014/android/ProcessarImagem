package br.edu.up.processarimageme2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  public void onClickVisualizar(View v){

    File sdCard = Environment.getExternalStoragePublicDirectory(
                         Environment.DIRECTORY_PICTURES);
    File arquivo = new File(sdCard, "pokemon.png");
    try {
      FileInputStream fis = new FileInputStream(arquivo);
      //BufferedInputStream bis = new BufferedInputStream(fis);
      Bitmap bitmap = BitmapFactory.decodeStream(fis);

      ImageView imageView = (ImageView) findViewById(R.id.imageView);
      imageView.setImageBitmap(bitmap);

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }


  }

  public void onClickBaixar(View v){

    try {
      URL url = new URL("https://drive.google.com/uc?export=view&id=0BwO0-hA9fDiUVG55bF9aYlJQTTQ");
      BaixarImagemTask task = new BaixarImagemTask();
      task.execute(url);

    } catch (MalformedURLException e) {
      e.printStackTrace();
    }


  }

  public class BaixarImagemTask extends AsyncTask<URL, String, Bitmap> {

    @Override
    protected Bitmap doInBackground(URL... urls) {

      URL url = urls[0];
      try {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        InputStream is = con.getInputStream();

        File sdCard = Environment.getExternalStoragePublicDirectory(
                                              Environment.DIRECTORY_PICTURES);
        File arquivo = new File(sdCard, "pokemon.png");
        FileOutputStream fos = new FileOutputStream(arquivo);
        BufferedOutputStream bos = new BufferedOutputStream(fos);

        byte[] buffer1k = new byte[1024];
        int qtdeBytesLidos = is.read(buffer1k);

        while (qtdeBytesLidos > 0){
          bos.write(buffer1k, 0, qtdeBytesLidos);
          qtdeBytesLidos = is.read(buffer1k);
        }
        bos.close();
        fos.close();
        is.close();

      } catch (IOException e) {
        e.printStackTrace();
      }
      return null;
    }
  }


}




